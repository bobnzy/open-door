// components/login-main-menu/login-main-menu.js

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    funItems:{
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转路径
    goToPages(e){
      switch (e.currentTarget.dataset.id) {
        case 1:
          // 需要判断是否登录(暂时未判断)
          wx.navigateTo({
            url: '/pages/profile/components/person-record/person-record',
          })
          break;
      
        default:
          break;
      }
    }
  }
})
