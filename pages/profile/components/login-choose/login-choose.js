const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

    // 获取用户信息
    getuserInfo(e) {
      app.globalData.userInfo = e.detail.userInfo
      wx.reLaunch({
        url: '/pages/profile/profile',
      })
    },

    // 跳转到手机登录页面
    goToLoginPhone() {
      wx.navigateTo({
        url: '/pages/profile/components/login-phone/login-phone',
      })
    }
  }
})