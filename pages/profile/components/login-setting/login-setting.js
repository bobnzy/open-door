const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    userinfo: []
  },

  /**
   * 组件的方法列表
   */
  methods: {
    clearUserInfo(){
      app.globalData.userInfo = null
      wx.reLaunch({
        url: '/pages/profile/profile',
      })
    }
  },
  lifetimes:{
    attached: function(){
      this.setData({
        userinfo: app.globalData.userInfo
      })
    }
  }
})
