// pages/profile/components/person-record/person-record.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    message(){
      wx.showModal({
        title: '提示',
        content: '确认呼叫此号码? 13993746557',
        cancelColor: "#07C160",
        confirmColor: "#07C160",
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  }
})
