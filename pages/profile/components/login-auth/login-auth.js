// components/login-auth/login-auth.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    goToChoose(){
      wx.navigateTo({
        url: '/pages/profile/components/login-choose/login-choose',
      })
    }
  }
})
