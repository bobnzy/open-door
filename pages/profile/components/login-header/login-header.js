// 获取应用根实例
const app = getApp()
Component({

  // 组件的属性列表
  properties: {},

  // 组件的初始数据
  data: {
    // 用户信息
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    isLogin: true
  },

  // 组件的方法列表
  methods: {

    // 跳转到设置页面
    goToSetting: function () {
      wx.navigateTo({
        url: '/pages/profile/components/login-setting/login-setting',
      })
    },

    // 跳转到选择页面
    goToAuth: function(){
      wx.navigateTo({
        url: '/pages/profile/components/login-auth/login-auth'
      })
    }
  },

  lifetimes: {
    attached: function () {
      if (app.globalData.userInfo) {
        this.setData({
          userInfo: app.globalData.userInfo,
          hasUserInfo: true
        })
      } else if (this.data.canIUse) {
        // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
        // 所以此处加入 callback 以防止这种情况
        app.userInfoReadyCallback = res => {
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      } else {
        // 在没有 open-type=getUserInfo 版本的兼容处理
        wx.getUserInfo({
          success: res => {
            app.globalData.userInfo = res.userInfo
            this.setData({
              userInfo: res.userInfo,
              hasUserInfo: true
            })
          }
        })
      }
    }
  },

})