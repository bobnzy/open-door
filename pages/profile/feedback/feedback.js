// pages/profile/feedback/feedback.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: '', // 当前点击的问题类型index对应值，用以改变样式
    questions: [ // 问题类型列表
      {
        value: "0",
        name: "意见建议"
      },
      {
        value: "1",
        name: "功能异常"
      },
      {
        value: "2",
        name: "使用帮助"
      },
      {
        value: "3",
        name: "其他问题"
      }
    ],
    textareaLength: 0, //文本域输入文本长度
    upImgNum: 0, // 当前上传图片数

    // 表单数据
    questionType: "", //问题类型
    description: "", // 问题文字描述
    describeImgs: [], // 上传问题截图列表

  },

  // 选择反馈问题类型
  radioPick(e) {
    // 获取点击的类型的index
    const index = e.currentTarget.dataset.index

    // // 表单值
    const QT = this.data.questions[index].name

    // 更改对应active值，样式变色
    this.setData({
      active: index,
      questionType: QT
    })
  },

  // 文本域输入
  textareaChange: function (e) {
    // console.log(e.detail.value.length)
    this.setData({
      textareaLength: e.detail.value.length
    })
  },
  // 文本域失焦
  textareaBlur: function (e) {
    // console.log(e.detail.value,"长度：",e.detail.value.length)
    const descriptionValue = e.detail.value
    this.setData({
      description: descriptionValue
    })
    // console.log(this.data)
  },

  /**
   * 上传图片
   */
  // 点击添加图片上传
  addImg: function () {
    if( this.data.upImgNum>=5) return //图片已满，什么都不做
    var that = this;
    wx.chooseImage({ //从本地相册选择图片或使用相机拍照
      count: 5, // 一次最多可以选择的图片数，默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // console.log(res)
        let uploadImgList = that.data.describeImgs 
        let imgNum=uploadImgList.length //当前已有图片数
        for (let i in res.tempFilePaths) {
          //  判断图片数是否大于上传上限5，没有就加入数组显示出来
          if (imgNum < 5) {
            uploadImgList.push({
              imgUrl: res.tempFilePaths[i]
            })
            imgNum++
          } else {
            wx.showToast({
              title: '最多5张！',
              icon: 'warn',
              duration: 2000
            })
            break;
          }
        }
        // console.log(uploadImgList)

        //前台显示
        that.setData({
          describeImgs: uploadImgList,
          upImgNum:imgNum
        })
        // console.log(that.data.describeImgs)

        // 上传到服务器：
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        // var tempFilePaths = res.tempFilePaths
        //  wx.uploadFile({
        //   url: 'http://172.16.3.247:8080/getData',
        //   filePath: tempFilePaths[0],
        //   name: 'file',
        //   success:function(res){
        //     //打印
        //     console.log(res.data)
        //   }
        // })
      }
    })
    /**
     * 三、PHP后端代码
     *    // 上传图片
     *     public function uploadimg()
     *     {
     *          $file = request()->file('file');
     *         if ($file) {
     *             $info = $file->move('public/upload/weixin/');
     *             if ($info) {
     *                 $file = $info->getSaveName();
     *                 $res = ['errCode'=>0,'errMsg'=>'图片上传成功','file'=>$file];
     *                 return json($res);
     *             }
     *         }
     *     }
     * ---------------------
     * 来源地址：https://www.php.cn/xiaochengxu-409188.html
     *      */

  },

  // 移除图片
  removeImg(e){
    const imgId=e.currentTarget.dataset.id
    this.data.describeImgs.splice(imgId,1) 
    this.setData({
      describeImgs:this.data.describeImgs,
      upImgNum:this.data.upImgNum-1
    })
  },

  // 表单提交
  formSubmit(e) {
    const data = {
      questionType: this.data.questionType, //问题类型
      description: this.data.description, // 问题文字描述
      describeImgs: this.data.describeImgs, // 上传问题截图列表
    }
    console.log('form发生了submit事件，携带数据为：', data)

    // 数据请求

  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '意见反馈'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})