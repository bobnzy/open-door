// pages/profile/profile.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 菜单数据
    menuList: [
      {
        iconUrl: "/assets/imgMenuList/订单.png",
        title: "我的订单",
        url: ""
      },
      {
        iconUrl: "/assets/imgMenuList/店长中心.png",
        title: "店长中心",
        url: ""
      },
      {
        iconUrl: "/assets/imgMenuList/优惠卷.png",
        title: "优惠卷",
        url: ""
      },
      {
        iconUrl: "/assets/imgMenuList/消息通知.png",
        title: "消息通知",
        url: ""
      }
    ],
    // 功能服务数据
    funItems: [
      {
        id: 0,
        iconUrl: "/assets/imgFunItems/小区.png",
        title: "我的小区",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 1,
        iconUrl: "/assets/imgFunItems/记录.png",
        title: "申请记录",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 2,
        iconUrl: "/assets/imgFunItems/物业缴费.png",
        title: "物业缴费",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 3,
        iconUrl: "/assets/imgFunItems/邀请.png",
        title: "邀请家人",
        url: "",
        badgeType: 'string',
        value: 'qqq'
      }, {
        id: 4,
        iconUrl: "/assets/imgFunItems/足迹.png",
        title: "访客授权",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 5,
        iconUrl: "/assets/imgFunItems/标签.png",
        title: "门卡管理",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 6,
        iconUrl: "/assets/imgFunItems/联系人.png",
        title: "联系物业",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 7,
        iconUrl: "/assets/imgFunItems/车牌.png",
        title: "绑定车牌",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 8,
        iconUrl: "/assets/imgFunItems/开门.png",
        title: "开门教程",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 9,
        iconUrl: "/assets/imgFunItems/消息.png",
        title: "用户反馈",
        url: "",
        badgeType: null,
        value: ''
      },
      {
        id: 10,
        iconUrl: "/assets/imgFunItems/红包.png",
        title: "亲邻有福利",
        url: "",
        badgeType: 'number',
        value: 99
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  },
  
})