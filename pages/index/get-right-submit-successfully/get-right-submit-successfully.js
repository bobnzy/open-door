// pages/get-right-submit-successfully/get-right-submit-successfully.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    steps:[
      {
        step:1,
        url:"/assets/images/get-right-submit-successfully/1.png"
      },
      {
        step:2,
        url:"/assets/images/get-right-submit-successfully/2.png"
      },
      {
        step:3,
        url:"/assets/images/get-right-submit-successfully/3.png"
      }
    ],
    test:"test word"
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 请求测试
  */
 btnTest1() {
  console.log("点击了这个btn",this.data.test);
  
  let data={
    id:"111",
    name:"张三"
  }

  //post请求
  wx.request({
    url: 'http://172.16.3.247:8081/getData',
    data:data,
    dataType:"json",
    method:"post",
    header: {
      'content-type': 'application/json' // 默认值
      // 'content-type':'application/x-www-form-urlencoded'
    },
    success: (result) => {
      console.log(result);
    },
    fail: (res) => {},
    complete: (res) => {},
  })

  // get请求

  // wx.request({
  //   // url: 'http://172.16.3.247:8081/getData?id=1111&name="aa"',
  //   url: 'http://172.16.3.247:8081/getData?',
  //   data:JSON.stringify(data),
  //   // data:data,
  //   dataType:"json",
  //   method:"get",
  //   success: (result) => {
  //     console.log(result);
  //   },
  //   fail: (res) => {},
  //   complete: (res) => {},
  // })
},
 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})