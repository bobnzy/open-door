//index.js
//获取应用实例
const app = getApp()
var http = require("../../utils/http");
Page({
  data: {
    _TestDotaLits: [],
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    adBotgoods: [{
        title: '欧斯卡王虾原装进口南美白虾仁鲜活海鲜水产大虾仁',
        img: '/assets/images/img-goos-01.jpg'
      },
      {
        title: '哈根达斯冰淇淋2个品脱+4个小纸杯组合装',
        img: '/assets/images/img-goos-02.jpg'
      },
      {
        title: '新西兰进口雪糕大桶装焦糖味冰激凌冷饮家庭装',
        img: '/assets/images/img-goos-03.jpg'
      },
      {
        title: '大西洋真鳕鱼新鲜宝宝辅食鳕鱼片海鲜',
        img: '/assets/images/img-goos-04.jpg'
      }
    ],
    adBotgoods2: [{
        title: '欧斯卡王虾原装进口南美白虾仁鲜活海鲜水产大虾仁',
        img: '/assets/images/img-goos-01.jpg'
      },
      {
        title: '哈根达斯冰淇淋2个品脱+4个小纸杯组合装',
        img: '/assets/images/img-goos-02.jpg'
      },
      {
        title: '新西兰进口雪糕大桶装焦糖味冰激凌冷饮家庭装',
        img: '/assets/images/img-goos-03.jpg'
      },
      {
        title: '大西洋真鳕鱼新鲜宝宝辅食鳕鱼片海鲜',
        img: '/assets/images/img-goos-04.jpg'
      }
    ]
  },
  getTestDota() {
    var that = this
    var prams = [{
        id: 1,
        title: "唱歌",
      },
      {
        id: 2,
        title: "跳舞",
      },
      {
        id: 3,
        title: "篮球",
      }
    ]
    http.postRequest("getData", prams,
      function (res) {
        let newdata = res.data;
        that.setData({
          _TestDotaLits: newdata
        })
        // console.log(that.data._TestDotaLits)
      },
      function (err) {})
  },
  onLoad: function () {
    // this.getTestDota()
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
    // console.log('页面刷新了')
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  onReachBottom() {
    console.log('用户上拉了')
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    this.setData({
      adBotgoods: this.data.adBotgoods.concat(this.data.adBotgoods2)
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 1000)
  },
  onPullDownRefresh() {
    console.log("下拉刷新");
    this.onLoad();
    wx.stopPullDownRefresh() //停止下拉刷新
  },
})