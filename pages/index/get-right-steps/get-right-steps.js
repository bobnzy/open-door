// pages/getrightsteps/getrightsteps.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    steps:[
      {
        imgUrl:"/assets/images/get-right-steps/1.png",
        msgTitle:"1.联系物业",
        msgText1:"前往社区物业管理处",
        msgText2:"联系管理人员提供授权二维码"
      },{
        imgUrl:"/assets/images/get-right-steps/2.png",
        msgTitle:"2.扫描授权二维码",
        msgText1:"微信扫描授权二维码",
        msgText2:"按管理人员要求填写申请资料"
      },{
        imgUrl:"/assets/images/get-right-steps/3.png",
        msgTitle:"3.提交认证",
        msgText1:"提交资料，立即获取开门权限",
        msgText2:"就可以开门了哦~"
      },
    ]
  },

  // “好，知道了”按钮，按下返回上一级页面
  getBack(){
wx.navigateBack({
  delta: 0,
})
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})