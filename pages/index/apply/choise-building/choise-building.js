// pages/index/apply/choise-building/choise-building.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    localVillage: "",
    // localbuilding: "",
    buildingData: [{
        id: 1,
        building: "楼栋1"
      },
      {
        id: 2,
        building: "楼栋2"
      },
      {
        id: 3,
        building: "楼栋3"
      }
    ]
  },
  // 获取当前小区
  getVillage() {
    let that = this
    wx.getStorage({
      key: 'village',
      success(res) {
        that.setData({
          localVillage: res.data
        })
      }
    })

  },
  // 切换小区，回到选择小区页面
  choiceOther() {
    wx.navigateBack({
      delta: 1,
    })
  },
  choiceBuilding(event) {
    let building = event.currentTarget.dataset.building
    wx.setStorage({
      data: building,
      key: 'building',
    })
    wx.redirectTo({
      url: '/pages/index/apply/apply',
    })
    wx.navigateBack({
      delta: 2,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '楼栋'
    })
    this.getVillage()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})