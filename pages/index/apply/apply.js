// pages/index/apply/apply.js
import WxValidate from "../../../utils/WxValidate.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 控制按钮是否禁用
    btnStatus: false,
    // 表单内容列表
    formTpyeList: [{
        id: 1,
        name: "city",
        title: "城市",
        text: "选择城市",
      },
      {
        id: 2,
        name: "village",
        title: "小区",
        text: "选择小区",
      },
      {
        id: 3,
        name: "building",
        title: "楼栋",
        text: "选择楼栋",
      },
      {
        id: 4,
        name: "house",
        title: "门牌号",
        text: "请输入",
      },
    ],
    // 表单数据
    formData: {
      city: '',
      village: '',
      building: '',
      house: ''
    }
  },
  // 获取本地缓存中的城市、小区、楼栋数据
  getStorageData() {
    let that = this
    let newCity = wx.getStorageSync('city')
    let newVillage = wx.getStorageSync('village')
    let newBuilding = wx.getStorageSync('building')
    this.setData({
      formData: {
        city: newCity,
        village: newVillage,
        building: newBuilding,
      }
    })
  },
  // 点击事件，进入选择地点页面
  goChoice() {
    wx.showToast({
      title: '定位中',
      icon: 'loading',
      duration: 1000,
      complete() {
        wx.navigateTo({
          url: '/pages/index/apply/choice-place/choice-place',
        })
      }
    })
  },
  // 输入框实时监听
  changeInput(e) {
    let formData = this.data.formData;
    // 获取到form的item的name值，对应到formData里的键值
    const key = e.currentTarget.dataset.name;
    // 根据这个键值，生成一个新的对象
    let obj = {
      [key]: e.detail.value
    }
    /// 合并两个对象，将值赋予进去
    obj = Object.assign(this.data.formData, obj);
    this.setData({
      formData: obj
    })
    //通过判断表单各条数据有内容，来控制按钮禁用状态
    if (obj.city.length && obj.village.length && obj.building.length && obj.house.length) {
      this.setData({
        btnStatus: true
      })
    } else {
      this.setData({
        btnStatus: false
      })
    }
  },
  // 表单提交按钮点击
  formSubmit: function (e) {
    console.log(e.detail.value)
    wx.navigateTo({
      url: '/pages/index/face/face',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '申请开门权限'
    })
    // this.initValidate();
    this.getStorageData()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})