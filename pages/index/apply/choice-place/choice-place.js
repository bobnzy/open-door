// pages/index/apply/choice-place/choice-place.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    localCity: "",
    localVillage: "",
    // 本地模拟json数据
    villageData: [{
        id: 1,
        place: "光华馨地"
      },
      {
        id: 2,
        place: "缤纷年华"
      },
      {
        id: 3,
        place: "滨河丽景"
      },
      {
        id: 4,
        place: "今日家园"
      },
      {
        id: 5,
        place: "双楠美邻"
      }
    ]
  },
  // 获取当前城市
  getCity() {
    // 假设获取的城市是成都市
    var newCity = "成都市"
    this.setData({
      localCity: newCity
    })
    wx.setStorage({
      key: 'city',
      data: newCity
    })
  },
  choiceReset() {
    wx.showToast({
      title: '正在加载',
      icon: 'loading',
      duration: 1000
    })
    this.getCity()
  },
  choiceOther() {
    wx.showToast({
      title: '正在切换',
      icon: 'loading',
      duration: 1000
    })
    var otherCity = "绵阳市"
    this.setData({
      localCity: otherCity
    })
    wx.setStorage({
      key: "city",
      data: otherCity
    })
  },
  choiceVillage(event) {
    let village = event.currentTarget.dataset.village
    console.log("选择的小区是：" + village)
    wx.setStorage({
      data: village,
      key: 'village',
    })
    wx.navigateTo({
      url: '/pages/index/apply/choise-building/choise-building',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '选择小区'
    })
    // 获取当前城市
    this.getCity()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})