Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 控制按钮是否禁用
    btnStatus: false,
    // 表单内容列表
    formTpyeList: [{
        id: 1,
        name: "city",
        title: "身份类型",
        text: "选择身份",
      },
      {
        id: 2,
        name: "village",
        title: "真实姓名",
        text: "请输入",
      },
      {
        id: 3,
        name: "building",
        title: "证件类型",
        text: "请选择",
      },
      {
        id: 4,
        name: "house",
        title: "证件号码",
        text: "请输入证件号码",
      },
    ],
  },
  changeInput(){},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '申请开门权限'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})