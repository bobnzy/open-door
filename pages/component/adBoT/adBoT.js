// pages/component/adBoT/adBoT.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    adBotgoods: [{
        title: '欧斯卡王虾原装进口南美白虾仁鲜活海鲜水产大虾仁',
        img: '/assets/images/img-goos-01.jpg'
      },
      {
        title: '哈根达斯冰淇淋2个品脱+4个小纸杯组合装',
        img: '/assets/images/img-goos-02.jpg'
      },
      {
        title: '新西兰进口雪糕大桶装焦糖味冰激凌冷饮家庭装',
        img: '/assets/images/img-goos-03.jpg'
      },
      {
        title: '大西洋真鳕鱼新鲜宝宝辅食鳕鱼片海鲜',
        img: '/assets/images/img-goos-04.jpg'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '加载中',
    })

    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})