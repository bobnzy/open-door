// pages/component/selSwiper/selSwiper.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperImg: [{
      id: 1,
      img: "/assets/images/swiper01.jpg"
    }, {
      id: 2,
      img: "/assets/images/swiper02.jpg"
    }, {
      id: 3,
      img: "/assets/images/swiper03.jpg"
    }, {
      id: 4,
      img: "/assets/images/swiper04.jpg"
    }, ],
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 5000,
    duration: 500
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})