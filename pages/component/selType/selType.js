// pages/component/selType/selType.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selTypeList: [{
        id: 1,
        img: "/assets/images/selType-img01.png",
        text: "本周新品"
      },
      {
        id: 2,
        img: "/assets/images/selType-img02.png",
        text: "限时抢购"
      },
      {
        id: 3,
        img: "/assets/images/selType-img03.png",
        text: "领劵中心"
      },
      {
        id: 4,
        img: "/assets/images/selType-img04.png",
        text: "到家服务"
      },
      {
        id: 5,
        img: "/assets/images/selType-img05.png",
        text: "智能小电"
      },
      {
        id: 6,
        img: "/assets/images/selType-img06.png",
        text: "热销美食"
      },
      {
        id: 7,
        img: "/assets/images/selType-img07.png",
        text: "美妆洗护"
      }, {
        id: 8,
        img: "/assets/images/selType-img08.png",
        text: "居家日用"
      }

    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})